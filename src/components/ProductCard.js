import { useState , useContext, useEffect} from 'react';
import { Card, Button, Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Products from '../pages/Products';
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import '../App.css';

// Aos 
import Aos from "aos";
import "aos/dist/aos.css";



export default function ProductCard({ productProp }) {

	useEffect(() => {
		Aos.init({});

	}, [])
	const {user,setUser} = useContext(UserContext)
	console.log(productProp)
	const { name, description, price, quantity, _id , cloudinary_id, picture } = productProp
	

	const addToCart = (data) => {
		

		if(user.id == null){
			
			Swal.fire({
					title: 'Please login first',
					icon: 'error',
					text: 'Then try again'
				})
		}else{



		console.log(data._id)
		fetch('https://secure-refuge-66531.herokuapp.com/users/cart/addToCart',{
			method: 'POST',
			headers: {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				cartItems : 
				{
					  "product" : data._id,
          			  "name": data.name,
          			  "quantity" : 1, 
            		  "price" : data.price

				}
			})

		})
		.then(res =>res.json())
		.then(data => {

		})

			Swal.fire({
					title:'Item has been added to cart!',
					icon:'success'
			})
			}

		
	}

	return (
		
		<Card className="mx-auto" data-aos="fade-up"  data-aos-anchor-placement="top-bottom"  data-aos-duration="1500">
			<Card.Body className="d-flex flex-column productCrd ">
				<Card.Title>{name}</Card.Title>
				<Image  className="photo" src={picture} rounded />
				<Card.Subtitle>Description</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<div className="bottom" >
				<Card.Subtitle>Price</Card.Subtitle>
				<Card.Text >{price}</Card.Text>
				</div>
				<Button id="prodBtn" className="button mt-auto" onClick={()=>addToCart(productProp)}>Add to Cart</Button>	

			</Card.Body>
		</Card>
		

	)
}

